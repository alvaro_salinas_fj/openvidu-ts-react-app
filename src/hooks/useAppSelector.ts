import { TypedUseSelectorHook, useSelector } from "react-redux";
import type { RootState } from "redux/rootReducer";

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
