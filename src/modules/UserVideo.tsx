import React from "react";
import { StreamManager } from "openvidu-browser";

import { OpenViduVideo } from "components/OpenViduVideo";

import "styles/modules/UserVideo.css";

type UserVideoProps = {
  streamManager: StreamManager;
};

export const UserVideo = ({ streamManager }: UserVideoProps): JSX.Element => {
  const getNicknameTag = () => {
    return streamManager.stream.connection.data;
  };

  return (
    <React.Fragment>
      {streamManager ? (
        <section className="streamcomponent">
          <OpenViduVideo streamManager={streamManager} />
          <div>
            <p>{getNicknameTag()}</p>
          </div>
        </section>
      ) : null}
    </React.Fragment>
  );
};
