import React, { useEffect, useRef } from "react";
import { StreamManager } from "openvidu-browser";

type OpenViduVideoProps = {
  streamManager: StreamManager;
};

export const OpenViduVideo = ({
  streamManager,
}: OpenViduVideoProps): JSX.Element => {
  const videoRef = useRef<HTMLVideoElement>(null);

  useEffect(() => {
    streamManager.addVideoElement(videoRef.current!);
  }, [streamManager]);

  return <video autoPlay={true} ref={videoRef} />;
};
