export interface UserEditionResponse {
  sessionId: string;
  createdAt: number;
  customSessionId: string;
  recording: boolean;
  mediaMode: string;
  recordingMode: string;
  defaultRecordingProperties: DefaultRecordingProperties;
  connections: Connections;
}

export interface Connections {
  numberOfElements: number;
  content: Content[];
}

export interface Content {
  connectionId: string;
  createdAt: number;
  role: string;
  serverData: string;
  token: string;
  clientData: null;
  subscribers: any[];
  publishers: any[];
}

export interface DefaultRecordingProperties {
  name: string;
  hasAudio: boolean;
  hasVideo: boolean;
  outputMode: string;
  ignoreFailedStreams: boolean;
}
