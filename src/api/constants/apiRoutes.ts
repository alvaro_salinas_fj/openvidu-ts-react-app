export const openviduServerUrl: string =
  process.env.REACT_APP_OPENVIDU_SERVER_URL || "https://localhost:3001";

export const createTokenRoute = "/api/v1.0/session";

export module SessionEndpoints {
  export const createSessionEndpoint = () => {
    return "v1.0/sessions";
  };

  export const getActiveSessionEndpoint = () => {
    return "v1.0/sessions";
  };

  export const closeSessionGracefullyEndpoint = (sessionName: string) => {
    return `v1.0/sessions/${sessionName}/close`;
  };

  export const getSessionIdEndpoint = (sessionName: string) => {
    return `v1.0/sessions/sessionId/${sessionName}`;
  };
}

export module ConnectionEndpoints {
  export const createConnectionEndpoint = () => {
    return "v1.0/connections/create";
  };

  export const forceDisconnectEndpoint = (
    connectionId: string,
    sessionName: string
  ) => {
    return `v1.0/connections/${connectionId}/session/${sessionName}/disconnect`;
  };
}
