import { createAsyncThunk } from "@reduxjs/toolkit";
import { forceDisconnect, newConnection } from "api/connections";
import { closeSession, createSession } from "api/sessions";
import {
  closeConnectionObject,
  createConnectionResponse,
  NewConnectionObject,
} from "redux/types/types";

export const createSessionAsync = createAsyncThunk(
  "session/createSessionAsync",
  async (sessionName: string) => {
    await createSession(sessionName);

    return sessionName;
  }
);

export const closeSessionAsync = createAsyncThunk(
  "session/closeSessionAsync",
  async (sessionName: string) => {
    await closeSession(sessionName);

    return;
  }
);

export const createConnectionAsync = createAsyncThunk(
  "connection/createConnectionAsync",
  async (object: NewConnectionObject) => {
    const response = await newConnection(object);
    const { token, connectionId } =
      (await response.json()) as createConnectionResponse;

    return { token, connectionId };
  }
);

export const closeConnectionAsync = createAsyncThunk(
  "connection/closeConnectionAsync",
  async (object: closeConnectionObject) => {
    const { connectionId, sessionName } = object;
    await forceDisconnect(connectionId, sessionName);

    return;
  }
);
