import { newConnection } from "api/connections";
import { OpenViduRoles } from "api/types/connections.types";
import logo from "assets/images/openvidu_grey_bg_transp_cropped.png";
import { useAppDispatch, useAppSelector } from "hooks";
import { UserVideo } from "modules/UserVideo";
import {
  ExceptionEvent,
  OpenVidu,
  Publisher,
  Session,
  StreamEvent,
  StreamManager,
  Subscriber,
} from "openvidu-browser";
import React, { FormEvent, useEffect, useState } from "react";
import { closeSessionAsync } from "redux/session/asyncActions";
import {
  removeConnectionId,
  removeVideoToken,
  setConnectionId,
  setVideoSessionName,
  setUserName,
  setVideoToken,
  setScreenSessionName,
} from "redux/session/reducer";
import "styles/pages/Home.css";

export const Home = (): JSX.Element => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    window.addEventListener("beforeunload", onbeforeunload);
    return () => {
      window.removeEventListener("beforeunload", onbeforeunload);
    };
  });

  useEffect(() => {
    dispatch(setUserName(""));
    dispatch(setVideoSessionName(""));
    dispatch(setScreenSessionName(""));
  }, [dispatch]);

  const videoSessionName = useAppSelector(
    (state) => state.session.videoSessionName
  );
  const screenSessionName = useAppSelector(
    (state) => state.session.screenSessionName
  );
  const userName = useAppSelector((state) => state.session.userName);
  const [mainStreamManager, setMainStreamManager] = useState<
    Publisher | Subscriber | undefined
  >(undefined);
  const [videoPublisher, setVideoPublisher] = useState<Publisher | undefined>(
    undefined
  );
  const [screenPublisher, setScreenPublisher] = useState<Publisher | undefined>(
    undefined
  );
  const [subscribers, setSubscribers] = useState<Subscriber[]>([]);
  const [videoSession, setVideoSession] = useState<Session | undefined>(
    undefined
  );
  const [screenSession, setScreenSession] = useState<Session | undefined>(
    undefined
  );
  const [screenAccepted, setScreenAccepted] = useState(false);

  const deleteSubscriber = (streamManager: StreamManager) => {
    const subscriberToDelete: Subscriber = streamManager as Subscriber;
    const subs = subscribers;

    let index = subs.indexOf(subscriberToDelete, 0);
    if (index > -1) {
      subs.splice(index, 1);
      setSubscribers(subs);
    }
  };

  const onbeforeunload = (e: any) => {
    leaveSession();
  };

  const changeUserName = (event: FormEvent<HTMLInputElement>) => {
    dispatch(setUserName(event.currentTarget.value));
  };

  const changeSessionName = (event: FormEvent<HTMLInputElement>) => {
    dispatch(setVideoSessionName(event.currentTarget.value));
    dispatch(setScreenSessionName(event.currentTarget.value));
  };

  const mainVideoStream = (stream: Publisher | Subscriber) => {
    if (mainStreamManager !== stream) {
      setMainStreamManager(stream);
    }
  };

  const createVideoSession = async () => {
    const wrapper = document.createElement("div");

    const OV = new OpenVidu();
    const newSession = OV.initSession();

    newSession.on("streamCreated", (event) => {
      const localEvent = event as StreamEvent;

      const subscriber = newSession.subscribe(localEvent.stream, wrapper);
      const subsArr = [...subscribers];
      subsArr.push(subscriber);

      setSubscribers(subsArr);
    });

    newSession.on("streamDestroyed", (event) => {
      const localEvent = event as StreamEvent;

      deleteSubscriber(localEvent.stream.streamManager);
    });

    newSession.on("exception", (exception) => {
      const exceptionEvent = exception as ExceptionEvent;

      console.warn(exceptionEvent);
    });

    setVideoSession(newSession);
    return newSession;
  };

  const createScreenSession = async () => {
    const OV = new OpenVidu();
    const newSession = OV.initSession();

    setScreenSession(newSession);
    return newSession;
  };

  const createConnection = async (
    sessionName: string,
    connectionUsername: string
  ) => {
    const tokenResponse = await newConnection({
      sessionName: sessionName,
      data: connectionUsername,
      role: OpenViduRoles.PUBLISHER,
      record: true,
    });

    if (tokenResponse.status === 404) {
      return;
    }

    const { token, connectionId } = await tokenResponse.json();

    dispatch(setVideoToken(token));
    dispatch(setConnectionId(connectionId));

    return token;
  };

  const joinSessionVideo = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const mySession = await createVideoSession();
    const tokenVideo = await createConnection(videoSessionName, userName);

    const OV = new OpenVidu();
    const videoWrapper = document.createElement("div");

    try {
      await mySession.connect(tokenVideo);

      const myVideoPublisher = OV.initPublisher(videoWrapper, {
        audioSource: undefined,
        videoSource: undefined,
        publishAudio: true,
        publishVideo: true,
        resolution: "640x480",
        frameRate: 30,
        insertMode: "APPEND",
        mirror: false,
      });

      mySession.publish(myVideoPublisher);

      setMainStreamManager(myVideoPublisher);
      setVideoPublisher(myVideoPublisher);

      setVideoSession(mySession);
    } catch (error) {
      console.error(error);
    }
  };

  const joinSessionScreen = async (e: FormEvent<HTMLInputElement>) => {
    e.preventDefault();

    const mySession = await createScreenSession();
    const tokenScreen = await createConnection(
      screenSessionName,
      screenSessionName
    );

    const OV = new OpenVidu();
    const screenWrapper = document.createElement("div");

    try {
      await mySession.connect(tokenScreen);

      const myScreenPublisher = OV.initPublisher(screenWrapper, {
        videoSource: "screen",
        resolution: "1280x720",
      });

      myScreenPublisher.once("accessAllowed", (event) => {
        myScreenPublisher.stream
          .getMediaStream()
          .getVideoTracks()[0]
          .addEventListener("ended", () => {
            setScreenAccepted(false);
            console.log('User pressed the "Stop sharing" button');
          });
        mySession.publish(myScreenPublisher);
        setScreenAccepted(true);
      });

      myScreenPublisher.once("accessDenied", (event) => {
        setScreenAccepted(false);
        console.warn("ScreenShare: Access Denied");
      });

      setScreenPublisher(myScreenPublisher);
      setScreenSession(mySession);
    } catch (error) {
      console.error(error);
    }
  };

  const leaveSession = async () => {
    setSubscribers([]);
    dispatch(setVideoSessionName(""));
    dispatch(setUserName(""));
    dispatch(removeVideoToken());
    dispatch(removeConnectionId());
    setMainStreamManager(undefined);
    setVideoPublisher(undefined);
    setVideoSession(undefined);

    if (videoSession) {
      await dispatch(closeSessionAsync(videoSessionName));
    }
    if (screenSession) {
      await dispatch(closeSessionAsync(screenSessionName));
    }
  };

  return (
    <React.Fragment>
      <div className="container">
        {videoSession === undefined ? (
          <div id="join">
            <div id="img-div">
              <img src={logo} alt="OpenVidu logo" />
            </div>
            <div id="join-dialog" className="jumbotron vertical-center">
              <h1> Join a video session </h1>
              <form className="form-group" onSubmit={joinSessionVideo}>
                <p>
                  <label>Participant: </label>
                  <input
                    className="form-control"
                    type="text"
                    id="userName"
                    value={userName}
                    onChange={changeUserName}
                    required
                  />
                </p>
                <p>
                  <label> Session: </label>
                  <input
                    className="form-control"
                    type="text"
                    id="sessionId"
                    value={videoSessionName}
                    onChange={changeSessionName}
                    required
                  />
                </p>
                <p className="text-center">
                  <input
                    className="btn btn-lg btn-success"
                    name="commit"
                    type="submit"
                    value="JOIN"
                  />
                </p>
              </form>
            </div>
          </div>
        ) : null}

        {videoSession !== undefined ? (
          <div id="session">
            <div id="session-header">
              <h1 id="session-title">{videoSessionName}</h1>
              <input
                className="btn btn-large btn-danger"
                type="button"
                id="buttonLeaveSession"
                onClick={leaveSession}
                value="Leave session"
              />
              <input
                className="btn btn-large btn-success"
                type="button"
                id="buttonLeaveSession"
                onClick={joinSessionScreen}
                value="join screen"
              />
            </div>

            {mainStreamManager !== undefined ? (
              <div id="main-video" className="col-md-6">
                <UserVideo streamManager={mainStreamManager} />
              </div>
            ) : null}
            <div id="video-container" className="col-md-6">
              {videoPublisher !== undefined ? (
                <div
                  className="stream-container col-md-6 col-xs-6"
                  onClick={() => mainVideoStream(videoPublisher)}
                >
                  <UserVideo streamManager={videoPublisher} />
                </div>
              ) : null}
              {screenAccepted && (
                <div className="stream-container col-md-6 col-xs-6">
                  <UserVideo streamManager={screenPublisher!} />
                </div>
              )}
              {/* {subscribers.map((sub, i) => (
                <div
                  key={i}
                  className="stream-container col-md-6 col-xs-6"
                  onClick={() => mainVideoStream(sub)}
                >
                  <UserVideo streamManager={sub} />
                </div>
              ))} */}
            </div>
          </div>
        ) : null}
      </div>
    </React.Fragment>
  );
};
