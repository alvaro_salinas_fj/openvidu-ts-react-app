import { ConnectionEndpoints } from "./constants/apiRoutes";
import { fetchHelper } from "./helpers";
import { OpenViduRoles } from "./types/connections.types";

export interface NewConnectionBody {
  sessionName: string;
  data: string;
  record?: boolean;
  role?: OpenViduRoles;
}

export const newConnection = async (body: NewConnectionBody) => {
  const endpoint = ConnectionEndpoints.createConnectionEndpoint();

  const response = await fetchHelper(endpoint, "POST", body);

  return response;
};

export const forceDisconnect = async (
  connectionId: string,
  sessionName: string
) => {
  const endpoint = ConnectionEndpoints.forceDisconnectEndpoint(
    connectionId,
    sessionName
  );

  const response = await fetchHelper(endpoint, "DELETE");

  return response;
};
