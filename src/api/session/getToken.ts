import { createTokenRoute, openviduServerUrl } from "api/constants/apiRoutes";

type GetTokenResponse = {
  token?: string;
  error?: string;
};

export const getToken = async (session: string): Promise<GetTokenResponse> => {
  const petitionBody = {
    sessionName: session,
  };

  try {
    const response = await fetch(openviduServerUrl + createTokenRoute, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },

      body: JSON.stringify(petitionBody),
    });

    const jsonResponse = await response.json();

    return {
      token: jsonResponse.token,
      error: undefined,
    };
  } catch (error) {
    console.log(error);
    return {
      token: undefined,
      error: "could'nt retrieve a token",
    };
  }
};
