import { combineReducers } from "@reduxjs/toolkit";

import SessionReducer from "redux/session/reducer";

const rootReducer = combineReducers({
  session: SessionReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
