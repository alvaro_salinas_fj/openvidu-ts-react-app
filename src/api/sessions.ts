import { SessionEndpoints } from "api/constants/apiRoutes";
import { fetchHelper } from "api/helpers";

export const createSession = async (sessionName: string) => {
  const endpoint = SessionEndpoints.createSessionEndpoint();

  const response = await fetchHelper(endpoint, "POST", { sessionName });

  return response;
};

export const getActiveSessions = async () => {
  const endpoint = SessionEndpoints.getActiveSessionEndpoint();

  const response = await fetchHelper(endpoint, "GET");

  return response;
};

export const closeSession = async (sessionName: string) => {
  const endpoint = SessionEndpoints.closeSessionGracefullyEndpoint(sessionName);

  const response = await fetchHelper(endpoint, "DELETE");

  return response;
};

export const getSessionIdBySessionName = async (sessionName: string) => {
  const endpoint = SessionEndpoints.getSessionIdEndpoint(sessionName);

  const response = await fetchHelper(endpoint, "GET");

  return response;
};
