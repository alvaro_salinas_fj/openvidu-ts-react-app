import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "redux/rootReducer";
import { closeSessionAsync, createSessionAsync } from "./asyncActions";

type sessionStateType = {
  status: "success" | "loading" | "failed";
  videoSessionName: string;
  screenSessionName: string;
  userName: string;
  videoToken: string | undefined;
  videoRecordingId: string | undefined;
  videoConnectionId: string | undefined;
  errorMessage: string | undefined;
};

const initialState: sessionStateType = {
  status: "success",
  videoSessionName: "",
  screenSessionName: "",
  userName: "",
  videoToken: undefined,
  videoRecordingId: undefined,
  videoConnectionId: undefined,
  errorMessage: undefined,
};

export const sessionSlice = createSlice({
  name: "session",
  initialState: initialState,
  reducers: {
    setVideoSessionName: (state, action: PayloadAction<string>) => {
      state.videoSessionName = action.payload;
    },
    removeVideoSessionName: (state) => {
      state.videoSessionName = "";
    },
    setScreenSessionName: (state, action: PayloadAction<string>) => {
      state.screenSessionName = action.payload + "-screen";
    },
    removeScreenSessionName: (state) => {
      state.screenSessionName = "";
    },
    setUserName: (state, action: PayloadAction<string>) => {
      state.userName = action.payload;
    },
    removeUserName: (state) => {
      state.userName = "";
    },
    setVideoToken: (state, action: PayloadAction<string>) => {
      state.videoToken = action.payload;
    },
    removeVideoToken: (state) => {
      state.videoToken = undefined;
    },
    setConnectionId: (state, action: PayloadAction<string>) => {
      state.videoConnectionId = action.payload;
    },
    removeConnectionId: (state) => {
      state.videoConnectionId = undefined;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      createSessionAsync.fulfilled,
      (state, action: PayloadAction<string>) => {
        state.videoToken = action.payload;
      }
    );
    builder.addCase(createSessionAsync.rejected, (state, action) => {
      state.errorMessage = action.error.message;
    });
    builder.addCase(closeSessionAsync.fulfilled, (state) => {
      state.videoToken = undefined;
      state.videoRecordingId = undefined;
      state.videoConnectionId = undefined;
    });
    builder.addCase(closeSessionAsync.rejected, (state, action) => {
      state.errorMessage = action.error.message;
    });
  },
});

export const {
  removeConnectionId,
  removeScreenSessionName,
  removeUserName,
  removeVideoSessionName,
  removeVideoToken,
  setConnectionId,
  setScreenSessionName,
  setUserName,
  setVideoSessionName,
  setVideoToken,
} = sessionSlice.actions;

export const selectVideoToken = (state: RootState) => state.session.videoToken;
export const selectVideoConnectionId = (state: RootState) =>
  state.session.videoConnectionId;

export default sessionSlice.reducer;
