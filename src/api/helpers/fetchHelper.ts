const baseUrl = process.env.REACT_APP_OPENVIDU_SERVER_URL;

type Method = "GET" | "POST" | "PUT" | "DELETE";

/**
 * Fetch helper function, returns a Promise with the non parsed response
 * @param endpoint The endpoint to be called
 * @param body The body of the request if exists
 * @param method is the method of the request, can be GET, POST, PUT or DELETE
 * @returns
 */
export const fetchHelper = async (
  endpoint: string,
  method: Method = "GET",
  body: object = {}
) => {
  let response: Response;
  const url = `${baseUrl}/${endpoint}`;

  if (method === "GET") {
    response = await fetch(url);

    return response;
  } else {
    response = await fetch(url, {
      method,
      headers: {
        "Content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
      body: JSON.stringify(body),
    });

    return response;
  }
};
