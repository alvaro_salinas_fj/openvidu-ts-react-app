import { OpenViduRoles } from "api/types/connections.types";

export interface createConnectionResponse {
  token: string;
  connectionId: string;
}

export interface closeConnectionObject {
  connectionId: string;
  sessionName: string;
}

export interface NewConnectionObject {
  sessionName: string;
  data: string;
  record?: boolean;
  role?: OpenViduRoles;
}
